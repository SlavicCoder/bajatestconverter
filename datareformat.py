# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 21:04:31 2022

@author: ar7fe
"""

import numpy as np
import csv

class DataReformat:

    def __init__(self):
        self.headers = ["Record #", "Time"]
        self.headersNP = None
        self.data = {}
        self.counter = 0

    def loadHeaders(self, filelist):
        for file in filelist:
            openedFile = open(file)
            for line in openedFile:
                line = line.strip()
                #If it's a 
                if line[0:4] == "SG_ ":
                    line = line[4:]
                    splitLine = line.split(":")
                    line = splitLine[0].strip()
                    self.headers.append(line)
          
        for header in self.headers:
            self.data[header] = list()
            
        np.array(self.headers)

    
    def loadData(self, filename):
        file = open(filename)
        allRecords = {}
        for line in file:
            
            #if(self.counter == 100):
                #break
            
            line = line.strip()
            fullLine = line.split(":")
            index = fullLine[0]
            if len(fullLine) > 1:
                data = fullLine[1]
            
            if index == "Time":
                if(self.counter > 0):    
                    self.saveToData(allRecords)
                    allRecords = {}
                self.counter += 1
                time = fullLine[1]
                time = time[:time.find("I")]
                time = time.strip()
                allRecords["Record #"] = self.counter
                allRecords["Time"] = time
            elif index in self.headers:
                allRecords[index] = data
                   
        
    #TODO: Fix it so it actually writes this at the correct index
    def saveToData(self, valuesToUpdate):
        for header in self.data.keys():
            if header not in valuesToUpdate.keys():
                self.data[header].append("")
            elif header in valuesToUpdate.keys():
                self.data[header].append(valuesToUpdate[header])
        
    def saveToFile(self):
        with open("C:\\Users\\ar7fe\\Downloads\\testRefactor2.csv", "w", encoding="UTF-8") as file:
            print("Writing to file")
            writer = csv.writer(file)
            testnp = np.arange(0, self.counter - 1)
            writer.writerow(self.headers)
            for index in np.nditer(testnp):
                rowToWrite = list()
                for header in self.data.keys():
                    try:
                        rowToWrite.append(self.data[header][index])
                    except:
                        print("Error appending to ds to write to file.")
                
                writer.writerow(rowToWrite)
                
            print(self.counter)
                
            
        
if __name__ == "__main__":
    reformat = DataReformat()
    dbcFiles = ["C:\\Users\\ar7fe\\Downloads\\Baja_Can.dbc", "C:\\Users\\ar7fe\\Downloads\\canmod-gnss.dbc"]
    reformat.loadHeaders(dbcFiles)
    reformat.loadData("C:\\Users\\ar7fe\\Downloads\\decoded.txt")
    reformat.saveToFile()